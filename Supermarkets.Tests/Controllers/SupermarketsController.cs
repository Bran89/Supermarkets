﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Supermarkets.Interfaces;
using Moq;
using System.Web.Http;
using Supermarkets.Models;
using System.Web.Http.Results;
using Supermarkets.Controllers;
using System.Linq;

namespace Supermarkets.Tests.Controllers
{
    /// <summary>
    /// Summary description for SupermarketsController
    /// </summary>
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetReturnsProductWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<ISupermarketRepository>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new Supermarket { Id = 42 });

            var controller = new SupermarketsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Supermarket>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);
        }

        // ----------------------------------------------------------------------------------------

        [TestMethod]
        public void GetReturnsNotFound()
        {
            // Arrange
            var mockRepository = new Mock<ISupermarketRepository>();
            var controller = new SupermarketsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Get(10);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }

        // ----------------------------------------------------------------------------------------

        [TestMethod]
        public void PostMethodSetsLocationHeader()
        {
            // Arrange
            var mockRepository = new Mock<ISupermarketRepository>();
            var controller = new SupermarketsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Post(new Supermarket { Id = 10, Label = "Supermarket1", City = "Novi Sad" });
            var createdResult = actionResult as CreatedAtRouteNegotiatedContentResult<Supermarket>;

            // Assert
            Assert.IsNotNull(createdResult);
            Assert.AreEqual("DefaultApi", createdResult.RouteName);
            Assert.AreEqual(10, createdResult.RouteValues["id"]);
        }

        // ----------------------------------------------------------------------------------------

        [TestMethod]
        public void PostReturnsMultipleObjects()
        {
            // Arrange
            List<SupermarketDTO> supermarkets = new List<SupermarketDTO>();
            supermarkets.Add(new SupermarketDTO { Label = "Supermarket1", Employees = 65 });
            supermarkets.Add(new SupermarketDTO { Label = "Supermarket2", Employees = 75 });

            var mockRepository = new Mock<ISupermarketRepository>();
            mockRepository.Setup(x => x.GetByEmployees(90, 50)).Returns(supermarkets.AsEnumerable());
            var controller = new SupermarketsController(mockRepository.Object);

            EmployeesFilter e = new EmployeesFilter
            {
                Min = 50,
                Max = 90
            };

            // Act
            IEnumerable<SupermarketDTO> result = controller.PostByEmployees(e);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(supermarkets.Count, result.ToList().Count);
            Assert.AreEqual(supermarkets.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(supermarkets.ElementAt(1), result.ElementAt(1));
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

    }
}
