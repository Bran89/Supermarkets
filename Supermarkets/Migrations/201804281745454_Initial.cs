namespace Supermarkets.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Chains",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Country = c.String(nullable: false, maxLength: 3),
                        YearEstablished = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Supermarkets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Label = c.String(nullable: false, maxLength: 20),
                        City = c.String(nullable: false),
                        YearOpened = c.Int(nullable: false),
                        Employees = c.Int(nullable: false),
                        ChainId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Chains", t => t.ChainId, cascadeDelete: true)
                .Index(t => t.ChainId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Supermarkets", "ChainId", "dbo.Chains");
            DropIndex("dbo.Supermarkets", new[] { "ChainId" });
            DropTable("dbo.Supermarkets");
            DropTable("dbo.Chains");
        }
    }
}
