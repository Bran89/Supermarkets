namespace Supermarkets.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Supermarkets.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Supermarkets.Models.SupermarketsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Supermarkets.Models.SupermarketsContext context)
        {
            //  This method will be called after migrating to the latest version.

            context.Chains.AddOrUpdate(c => c.Id,
                new Chain() { Id = 1, Country = "SLO", Name = "Mercator", YearEstablished = 1985},
                new Chain() { Id = 2, Country = "SRB", Name = "Univerexport", YearEstablished = 1993},
                new Chain() { Id = 3, Country = "CRO", Name = "IDEA", YearEstablished = 1996}
                );

            context.Supermarkets.AddOrUpdate(s => s.Id,
                new Supermarket() { Id = 1, Label = "MerLim", City = "Novi Sad", Employees = 90, YearOpened = 2011, ChainId = 1},
                new Supermarket() { Id = 2, Label = "UniSpens", City = "Novi Sad", Employees = 50, YearOpened = 2016, ChainId = 2},
                new Supermarket() { Id = 3, Label = "UniFut", City = "Novi Sad", Employees = 90, YearOpened = 2013, ChainId = 2},
                new Supermarket() { Id = 4, Label = "IdeaBul", City = "Novi Sad", Employees = 75, YearOpened = 2015, ChainId = 3}
                );
        }
    }
}
