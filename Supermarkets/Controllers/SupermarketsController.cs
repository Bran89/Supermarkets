﻿using Supermarkets.Interfaces;
using Supermarkets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Supermarkets.Controllers
{
    public class SupermarketsController : ApiController
    {
        ISupermarketRepository _repository { get; set; }

        public SupermarketsController(ISupermarketRepository repository)
        {
            _repository = repository;
        }

        // GET api/supermarkets
        [ResponseType(typeof(SupermarketDTO))]
        public IEnumerable<SupermarketDTO> Get()
        {
            return _repository.GetAll().OrderByDescending(s => s.YearOpened);
        }

        // GET api/supermarkets/{id}
        [ResponseType(typeof(Supermarket))]
        public IHttpActionResult Get(int id)
        {
            var supermarket = _repository.GetById(id);
            if (supermarket == null)
            {
                return NotFound();
            }
            return Ok(supermarket);
        }

        // GET api/supermarkets?employees={value}
        [ResponseType(typeof(SupermarketDTO))]
        public IEnumerable<SupermarketDTO> GetByEmployees(int employees)
        {
            return _repository.GetByEmployees(employees);
        }

        // POST api/supermarkets
        public IHttpActionResult Post(Supermarket supermarket)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Add(supermarket);
            return CreatedAtRoute("DefaultApi", new { id = supermarket.Id }, supermarket);
        }

        // POST api/search
        [Route("api/search")]
        [ResponseType(typeof(SupermarketDTO))]
        public IEnumerable<SupermarketDTO> PostByEmployees([FromBody] EmployeesFilter e)
        {
            return _repository.GetByEmployees(e.Max, e.Min);
        }

        // PUT api/supermarkets/{id}
        public IHttpActionResult Put(int id, Supermarket supermarket)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != supermarket.Id)
            {
                return BadRequest();
            }

            try
            {
                _repository.Update(supermarket);
            }
            catch
            {
                return BadRequest();
            }

            return Ok(supermarket);
        }

        // DELETE api/supermarkets/{id}
        public IHttpActionResult Delete(int id)
        {
            var supermarket = _repository.GetById(id);
            if (supermarket == null)
            {
                return NotFound();
            }

            _repository.Delete(supermarket);
            return Ok();
        }

    }
}
