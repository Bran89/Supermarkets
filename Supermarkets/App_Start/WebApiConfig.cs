﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Supermarkets.Interfaces;
using Supermarkets.Models;
using Supermarkets.Repository;
using Supermarkets.Resolver;
using Unity;
using Unity.Lifetime;

namespace Supermarkets
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            Mapper.Initialize(cfg =>{
                cfg.CreateMap<Supermarket, SupermarketDTO>(); // automatically maps Chain.Name to ChainName
            });

            // Unity
            var container = new UnityContainer();
            container.RegisterType<ISupermarketRepository, SupermarketRepository>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);
        }
    }
}
