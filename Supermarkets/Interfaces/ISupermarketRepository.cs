﻿using Supermarkets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supermarkets.Interfaces
{
    public interface ISupermarketRepository
    {
        IEnumerable<SupermarketDTO> GetAll();
        Supermarket GetById(int id);
        IEnumerable<SupermarketDTO> GetByEmployees(int max, int min = 0);
        void Add(Supermarket supermarket);
        void Update(Supermarket supermarket);
        void Delete(Supermarket supermarket);
    }
}
