﻿using Supermarkets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Supermarkets.Interfaces
{
    public interface IChainRepository
    {
        IEnumerable<Chain> GetAll();
        Chain GetById(int id);
        void Add(Chain chain);
        void Update(Chain chain);
        void Delete(Chain chain);
    }
}
