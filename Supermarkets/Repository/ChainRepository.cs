﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using AutoMapper.QueryableExtensions;
using Supermarkets.Interfaces;
using Supermarkets.Models;

namespace Supermarkets.Repository
{
    public class ChainRepository : IDisposable, IChainRepository
    {
        private SupermarketsContext db = new SupermarketsContext();

        public void Add(Chain chain)
        {
            db.Chains.Add(chain);
            db.SaveChanges();
        }

        public void Update(Chain chain)
        {
            db.Entry(chain).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(Chain chain)
        {
            db.Chains.Remove(chain);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                db = null;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Chain> GetAll()
        {
            return db.Chains;
        }

        public Chain GetById(int id)
        {
            return db.Chains.FirstOrDefault(c => c.Id == id);
        }
    }
}