﻿using Supermarkets.Interfaces;
using Supermarkets.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using AutoMapper.QueryableExtensions;

namespace Supermarkets.Repository
{
    public class SupermarketRepository : IDisposable, ISupermarketRepository
    {
        private SupermarketsContext db = new SupermarketsContext();

        public IEnumerable<SupermarketDTO> GetAll()
        {
            return db.Supermarkets.ProjectTo<SupermarketDTO>();
        }

        public Supermarket GetById(int id)
        {
            return db.Supermarkets.Include(c => c.Chain).FirstOrDefault(s => s.Id == id);
        }

        public IEnumerable<SupermarketDTO> GetByEmployees(int max, int min = 0)
        {
            return db.Supermarkets.Include(c => c.Chain).Where(s => s.Employees <= max && s.Employees >= min).ProjectTo<SupermarketDTO>();
        }

        public void Add(Supermarket supermarket)
        {
            db.Supermarkets.Add(supermarket);
            db.SaveChanges();
        }

        public void Update(Supermarket supermarket)
        {
            db.Entry(supermarket).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(Supermarket supermarket)
        {
            db.Supermarkets.Remove(supermarket);
            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
                db = null;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}