﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Supermarkets.Models
{
    public class SupermarketDTO
    {
        public string Label { get; set; }
        public string ChainName { get; set; }
        public string ChainCountry { get; set; }
        public int YearOpened { get; set; }
        public int Employees { get; set; }
    }
}