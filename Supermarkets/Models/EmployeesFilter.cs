﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Supermarkets.Models
{
    public class EmployeesFilter
    {
        public int Min { get; set; }
        public int Max { get; set; }
    }
}