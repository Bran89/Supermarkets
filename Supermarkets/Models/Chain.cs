﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Supermarkets.Models
{
    public class Chain
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [StringLength(3, MinimumLength = 3)]
        public string Country { get; set; }
        [Required]
        [Range(1, 2010)]
        public int YearEstablished { get; set; }
    }
}