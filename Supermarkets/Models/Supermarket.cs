﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Supermarkets.Models
{
    public class Supermarket
    {
        public int Id { get; set; }
        [Required]
        [StringLength(20)]
        public string Label { get; set; }
        [Required]
        public string City { get; set; }
        [Range(2010, 2018)]
        public int YearOpened { get; set; }
        [Range(1, 100)]
        public int Employees { get; set; }
        // Foreign Key
        [Required]
        public int ChainId { get; set; }
        // Navigation property
        public Chain Chain { get; set; }
    }
}