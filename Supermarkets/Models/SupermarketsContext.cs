﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Supermarkets.Models
{
    public class SupermarketsContext : DbContext
    {
        public SupermarketsContext(): base("name=SupermarketsContext") { }

        public DbSet<Chain> Chains { get; set; }
        public DbSet<Supermarket> Supermarkets { get; set; }
    }
}